package com.example.a20220510_chimezieorjiunegbu_nycschools.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220510_chimezieorjiunegbu_nycschools.R
import com.example.a20220510_chimezieorjiunegbu_nycschools.model.School

class NycSchoolAdapter (
    private val clickAdapter: NycClickAdapter,
    private val schoolList: MutableList<School> = mutableListOf()
        ) : RecyclerView.Adapter<NycSchoolViewHolder>(){

    fun addList(school: List<School>){
        schoolList.clear()
        schoolList.addAll(school)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NycSchoolViewHolder =
        NycSchoolViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false))


    override fun onBindViewHolder(holder: NycSchoolViewHolder, position: Int) {
        val school = schoolList[position]
        holder.bind(school, clickAdapter)
    }

    override fun getItemCount(): Int = schoolList.size


}

class NycSchoolViewHolder(
    private val schoolView: View
): RecyclerView.ViewHolder(schoolView){

    private val schoolName: TextView = schoolView.findViewById(R.id.schoolName)
    private val borough: TextView = schoolView.findViewById(R.id.borough)
    private val city: TextView = schoolView.findViewById(R.id.city)
    private val scoresButton: Button = schoolView.findViewById(R.id.scoresButton)

    fun bind(school: School, clickAdapter: NycClickAdapter){

        schoolName.text = school.schoolName
        borough.text = school.dbn
        city.text = school.city

        scoresButton.setOnClickListener {
            clickAdapter.onSchoolClicked(school)
        }
    }
}