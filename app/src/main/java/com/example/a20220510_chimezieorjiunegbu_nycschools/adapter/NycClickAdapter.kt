package com.example.a20220510_chimezieorjiunegbu_nycschools.adapter

import com.example.a20220510_chimezieorjiunegbu_nycschools.model.School

interface NycClickAdapter {
    fun onSchoolClicked(school: School)
}