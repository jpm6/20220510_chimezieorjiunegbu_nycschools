package com.example.a20220510_chimezieorjiunegbu_nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220510_chimezieorjiunegbu_nycschools.ApiService.SchoolRepository
import com.example.a20220510_chimezieorjiunegbu_nycschools.ApiService.SchoolState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NycViewModel @Inject constructor(
    private val nycRepository: SchoolRepository,
    private val ioDispatcher: CoroutineDispatcher
) : ViewModel() {

    var schoolDbn: String = ""
    var schoolName: String = ""

    private val _nycSchoolLiveData: MutableLiveData<SchoolState> = MutableLiveData(SchoolState.LOADING)
    val nycSchoolLiveData: LiveData<SchoolState> get() = _nycSchoolLiveData

    private val _scoresLiveData: MutableLiveData<SchoolState> = MutableLiveData(SchoolState.LOADING)
    val scoresLiveData: LiveData<SchoolState> get() = _scoresLiveData

    fun getSchools() {
        viewModelScope.launch(ioDispatcher) {
                nycRepository.getSchools()
                    .collect{ state ->
                        _nycSchoolLiveData.postValue(state)
                    }
        }
    }

    fun getScores() {
        viewModelScope.launch(ioDispatcher) {
                nycRepository.getSatScores()
                    .collect{ state ->
                        _scoresLiveData.postValue(state)
                    }
        }
    }

    fun resetState() {
        _nycSchoolLiveData.postValue(SchoolState.LOADING)
        _scoresLiveData.postValue(SchoolState.LOADING)
    }

}