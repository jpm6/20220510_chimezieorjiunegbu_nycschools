package com.example.a20220510_chimezieorjiunegbu_nycschools.model

import com.google.gson.annotations.SerializedName

data class School(
    @SerializedName("city")
    val city: String,
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("school_email")
    val schoolEmail: String,
    @SerializedName("phone_number")
    val phoneNumber: String,
    @SerializedName("overview_paragraph")
    val overviewParagraph: String,
    @SerializedName("latitude")
    val latitude: String,
    @SerializedName("location")
    val location: String,
    @SerializedName("longitude")
    val longitude: String,
    @SerializedName("website")
    val website: String,
    @SerializedName("zip")
    val zip: String
)
