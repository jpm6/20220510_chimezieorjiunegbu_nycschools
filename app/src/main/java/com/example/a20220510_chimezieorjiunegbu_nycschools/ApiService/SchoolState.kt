package com.example.a20220510_chimezieorjiunegbu_nycschools.ApiService

sealed class SchoolState {
    object LOADING : SchoolState()
    data class SUCCESS<T>(val data: T) : SchoolState()
    data class ERROR(val error: Exception) : SchoolState()
}
