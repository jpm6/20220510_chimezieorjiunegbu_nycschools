package com.example.a20220510_chimezieorjiunegbu_nycschools.ApiService

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

interface SchoolRepository {
    fun getSchools(): Flow<SchoolState>
    fun getSatScores(): Flow<SchoolState>
}

class SchoolRepositoryImpl @Inject constructor(
    private val apiService: NycApi
) : SchoolRepository {

    override fun getSchools(): Flow<SchoolState> = flow {
        emit(SchoolState.LOADING)
        kotlinx.coroutines.delay(500)

        try {
            val response = apiService.getNycSchools()
            if(response.isSuccessful) {
                response.body()?.let {
                    emit(SchoolState.SUCCESS(it))
                } ?: throw Exception("Error null reesponse")
            } else {
                val error = response.errorBody()?.string() ?: "Error failure response"
                throw Exception(error)
            }
        }catch (e: Exception) {
            emit(SchoolState.ERROR(e))
        }
    }

    override fun getSatScores(): Flow<SchoolState> =  flow {
        emit(SchoolState.LOADING)
        kotlinx.coroutines.delay(500)

        try {
            val response = apiService.getSatSchoolScore()
            if(response.isSuccessful) {
                response.body()?.let {
                    emit(SchoolState.SUCCESS(it))
                } ?: throw Exception("Error null reesponse")
            } else {
                val error = response.errorBody()?.string() ?: "Error failure response"
                throw Exception(error)
            }
        }catch (e: Exception) {
            emit(SchoolState.ERROR(e))
        }
    }
}