package com.example.a20220510_chimezieorjiunegbu_nycschools.ApiService

import com.example.a20220510_chimezieorjiunegbu_nycschools.model.School
import com.example.a20220510_chimezieorjiunegbu_nycschools.model.SchoolSAT
import retrofit2.Response
import retrofit2.http.GET

interface NycApi {

    @GET(SCHOOLS)
    suspend fun getNycSchools(): Response<List<School>>

    @GET(SCHOOL_SCORE)
    suspend fun getSatSchoolScore(): Response<List<SchoolSAT>>

    companion object {
        const val URL_BASE = "https://data.cityofnewyork.us/resource/"
        private const val SCHOOLS = "s3k6-pzi2.json"
        private const val SCHOOL_SCORE = "f9bf-2cp4.json"
    }
}