package com.example.a20220510_chimezieorjiunegbu_nycschools.di

import com.example.a20220510_chimezieorjiunegbu_nycschools.ApiService.SchoolRepository
import com.example.a20220510_chimezieorjiunegbu_nycschools.ApiService.SchoolRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    abstract fun providesRepository(schoolRepositoryImpl: SchoolRepositoryImpl): SchoolRepository
}