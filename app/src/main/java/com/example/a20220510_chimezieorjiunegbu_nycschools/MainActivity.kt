package com.example.a20220510_chimezieorjiunegbu_nycschools

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.a20220510_chimezieorjiunegbu_nycschools.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val binding by lazy{
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }
}