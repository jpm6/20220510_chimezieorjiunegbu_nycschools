package com.example.a20220510_chimezieorjiunegbu_nycschools.views

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.a20220510_chimezieorjiunegbu_nycschools.ApiService.SchoolState
import com.example.a20220510_chimezieorjiunegbu_nycschools.R
import com.example.a20220510_chimezieorjiunegbu_nycschools.databinding.FragmentNycScoresBinding
import com.example.a20220510_chimezieorjiunegbu_nycschools.model.SchoolSAT
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NycScoresFragment : BaseFragment() {

    private val binding by lazy{
        FragmentNycScoresBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        nycViewModel.scoresLiveData.observe(viewLifecycleOwner){ state ->
            when(state){
                is SchoolState.LOADING ->{
                    binding.scoresProgressBar.visibility = View.VISIBLE
                }
                is SchoolState.SUCCESS<*> ->{
                    val allScores = state.data as List<SchoolSAT>
                    val schoolScores = getSchoolScores(allScores, nycViewModel.schoolDbn, nycViewModel.schoolName)
                    binding.scoresProgressBar.visibility = View.GONE
                    initializeUI(schoolScores)
                }
                is SchoolState.ERROR ->{
                    val errorResults = getSchoolScores(listOf(), nycViewModel.schoolDbn, nycViewModel.schoolName)
                    initializeUI(errorResults)
                    binding.scoresProgressBar.visibility = View.GONE

                    AlertDialog.Builder(requireContext())
                        .setTitle("ERROR")
                        .setMessage(state.error.localizedMessage)
                        .setNegativeButton("DISMISS") { dialog, _ ->
                            dialog.dismiss()
                            requireActivity().supportFragmentManager.beginTransaction()
                                .replace(R.id.fragment_container, NycFragment.newInstance())
                                .addToBackStack(null)
                                .commit()
                        }
                        .create()
                        .show()
                }
            }
        }

        nycViewModel.getScores()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun getSchoolScores(scores: List<SchoolSAT>, dbn : String, schoolName : String): SchoolSAT {
        for(school in scores){
            if(school.dbn == dbn){
                return school
            }
        }
        return SchoolSAT(
            dbn = dbn,
            numOfTesters = "Error",
            readingScore = "0",
            mathScore = "0",
            writingScore = "0",
            schoolName = schoolName,
        )
    }

    private fun initializeUI(scores: SchoolSAT){
        binding.schoolName.text = scores.schoolName

        val testTakers = getString(R.string.numberOfTesters, scores.numOfTesters)
        binding.numberOfTesters.text = testTakers

        val criticalReadingScore = getString(R.string.readingScore, scores.readingScore)
        binding.readingScore.text = criticalReadingScore

        val mathScore = getString(R.string.mathScore, scores.mathScore)
        binding.mathScore.text = mathScore

        val writingScore = getString(R.string.writingScore, scores.writingScore)
        binding.writingScore.text = writingScore

    }

    override fun onStop() {
        super.onStop()
        nycViewModel.scoresLiveData.removeObservers(viewLifecycleOwner)
        nycViewModel.resetState()
    }

    companion object {
        fun newInstance() = NycScoresFragment()
    }


}