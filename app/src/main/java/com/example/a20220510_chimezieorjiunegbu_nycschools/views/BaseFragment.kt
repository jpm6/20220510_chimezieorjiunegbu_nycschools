package com.example.a20220510_chimezieorjiunegbu_nycschools.views

import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.a20220510_chimezieorjiunegbu_nycschools.viewmodel.NycViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
open class BaseFragment: Fragment() {
    val nycViewModel : NycViewModel by activityViewModels()
}