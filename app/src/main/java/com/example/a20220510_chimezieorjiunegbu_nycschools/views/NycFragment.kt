package com.example.a20220510_chimezieorjiunegbu_nycschools.views

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220510_chimezieorjiunegbu_nycschools.ApiService.SchoolState
import com.example.a20220510_chimezieorjiunegbu_nycschools.R
import com.example.a20220510_chimezieorjiunegbu_nycschools.adapter.NycClickAdapter
import com.example.a20220510_chimezieorjiunegbu_nycschools.adapter.NycSchoolAdapter
import com.example.a20220510_chimezieorjiunegbu_nycschools.databinding.FragmentNycBinding
import com.example.a20220510_chimezieorjiunegbu_nycschools.model.School
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NycFragment : BaseFragment(), NycClickAdapter {

    private val binding by lazy{
        FragmentNycBinding.inflate(layoutInflater)
    }

    private val nycSchoolAdapter by lazy{
        NycSchoolAdapter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter = nycSchoolAdapter
        }

        nycViewModel.nycSchoolLiveData.observe(viewLifecycleOwner){ state ->
            when(state){
                is SchoolState.LOADING ->{
                    binding.progressBar.visibility = View.VISIBLE
                    binding.recyclerView.visibility = View.GONE
                }
                is SchoolState.SUCCESS<*> ->{
                    val school = state.data as List<School>
                    nycSchoolAdapter.addList(school)

                    binding.progressBar.visibility = View.GONE
                    binding.recyclerView.visibility = View.VISIBLE
                }
                is SchoolState.ERROR ->{
                    binding.progressBar.visibility = View.GONE
                    binding.recyclerView.visibility = View.GONE
                    AlertDialog.Builder(requireContext())
                        .setTitle("ERROR")
                        .setMessage(state.error.localizedMessage)
                        .setPositiveButton("RETRY") { _ , _ ->
                            nycViewModel.getSchools()
                        }
                        .setNegativeButton("DISMISS") { dialog, _ ->
                            dialog.dismiss()
                        }
                        .create()
                        .show()
                }
            }
        }

        nycViewModel.getSchools()

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onSchoolClicked(school: School) {
        nycViewModel.schoolDbn = school.dbn
        nycViewModel.schoolName = school.schoolName
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, NycScoresFragment.newInstance())
            .addToBackStack(null)
            .commit()
    }

    override fun onStop() {
        super.onStop()
        nycViewModel.nycSchoolLiveData.removeObservers(viewLifecycleOwner)
        nycViewModel.resetState()
    }

    companion object {
        fun newInstance() = NycFragment()
    }


}